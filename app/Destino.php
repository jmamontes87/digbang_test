<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destino extends Model
{
    protected $table = 'destino';

    protected $fillable = [
        'destino'
    ];

    public function reglas()
    {
        return $this->hasMany('App\Regla');
    }

    public function movimientos()
    {
        return $this->hasMany('App\Movimiento');
    }
}
