<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regla extends Model
{
    protected $table = 'regla';

    protected $fillable = [
        'destino_id',
        'movimiento_tipo_id',
        'comision',
        'denominacion'
    ];

    public function destino()
    {
        return $this->hasOne('App\Destino', 'id', 'destino_id');
    }

    public function movimiento_tipo()
    {
        return $this->hasOne('App\MovimientoTipo', 'id', 'movimiento_tipo_id');
    }

    public function movimiento_regla()
    {
        return $this->belongsToMany('App\Movimiento')->withPivot();
    }
}
