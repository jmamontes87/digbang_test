<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteMultinacional extends Model
{
    protected $table = 'cliente_persona_juridica';

    protected $fillable = [
        'razon_social',
    	'nombre_fantasia',
    	'pais_casa_central',
    	'email_responsable',
    	'nombre_responsable',
    	'cantidad_empleados',
    ];

    public function clientes()
    {
        return $this->hasMany('App\Cliente');
    }
    
}
