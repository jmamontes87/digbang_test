<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Cuenta;

class realizar_cobros extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:realizar-cobros-mensuales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Realizar cobros mensuales';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cuentas = Cuenta::all();

        foreach ($cuentas as $cuenta) {

            $comision = 0;
            //cobro por movimiento
            foreach ($cuenta->movimientos as $movimiento) {

                if($movimiento->importe >= 1000){
                    //armar proceso libreria externa
                    //https://github.com/digbang/exercise-fraud-service
                }
                //multinacional
                if($cuenta->tipo_cliente_id == 1){
                    $comision = $comision + $movimiento->importe * 0.5;
                //pyme
                }elseif($cuenta->tipo_cliente_id == 2){
                    $comision = $comision + $movimiento->importe * 0.01;
                //persona
                }elseif($cuenta->tipo_cliente_id == 3){
                    $comision = $comision + $movimiento->importe * 0.05;
                }
            }
            
            if($cuenta->cuentaTipo->denominacion == 'classic'){
                //cuenta persona
                if($cuenta->tipo_cliente_id == 3){
                    $cuenta->saldo = $cuenta->saldo + 100;
                //cuenta empresa
                }else{
                    $cuenta->saldo = $cuenta->saldo + 1000;
                }
            }elseif($cuenta->cuentaTipo->denominacion == 'gold'){

                $cuenta->saldo = $cuenta->saldo + $comision; 
            }


            $cuenta->save();
        }
    }
}
