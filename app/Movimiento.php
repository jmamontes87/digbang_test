<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
    protected $table = 'movimiento';

    protected $fillable = [
        'movimiento_tipo_id',
        'destino_id',
        'cuenta_id',
        'importe'
    ];

    public function cuenta()
    {
        return $this->hasOne('App\Cuenta', 'id', 'cuenta_id');
    }

    public function destino()
    {
        return $this->hasOne('App\Destino', 'id', 'destino_id');
    }

    public function movimiento_tipo()
    {
        return $this->hasOne('App\MovimientoTipo', 'id', 'movimiento_tipo_id');
    }

    public function movimiento_regla()
    {
        return $this->belongsToMany('App\Regla')->withPivot();
    }
        
}
