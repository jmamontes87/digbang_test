<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientePersona extends Model
{
    protected $table = 'cliente_persona';

    protected $fillable = [
        'nombre',
    	'apellido',
    	'fecha_nacimiento',
    	'nacionalidad',
    ];

    public function clientes()
    {
        return $this->hasMany('App\Cliente');
    }
}
