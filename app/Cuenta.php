<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    protected $table = 'cuenta';

    protected $fillable = [
        'balance',
        'cliente_id',
        'saldo',
        'cuenta_tipo_id',
        'tipo_cliente_id'
    ];

    public function movimientos()
    {
        return $this->hasMany('App\Movimiento');
    }

    public function cliente()
    {
        return $this->hasOne('App\Cliente', 'id', 'cliente_id');
    }

    public function cuentaTipo()
    {
        return $this->hasOne('App\CuentaTipo', 'id', 'cuenta_tipo_id');
    }
}
