<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\MovimientoTipo;
use App\Regla;
use App\Destino;
use App\Cliente;
use App\Cuenta;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cliente = Cliente::where('user_id','=', Auth::user()->id)->first();
        $cuenta = Cuenta::where('cliente_id', '=', $cliente->id)->first();

        $destinos = Destino::all()->pluck('destino','id');
        $movimiento_tipos = MovimientoTipo::all()->pluck('tipo','id'); 

        return view('index', compact('destinos','movimiento_tipos', 'cliente', 'cuenta'));
    }
}
