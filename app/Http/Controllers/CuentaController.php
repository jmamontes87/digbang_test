<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Regla;
use App\Cuenta;
use App\Cliente;
use App\MovimientoTipo;
use App\Movimiento;

class CuentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $movimiento = Movimiento::find($id);

        $cliente = Cliente::where('user_id','=', Auth::user()->id)->first();

        $reglas = Regla::where('movimiento_tipo_id','=', $movimiento->movimiento_tipo_id)
                       ->where('destino_id','=', $movimiento->destino_id)
                       ->get();
        $cuenta = Cuenta::where('cliente_id', '=', $cliente->id)->first();

        foreach ($reglas as $regla) {
            
            $movimiento_tipo = MovimientoTipo::find($regla->movimiento_tipo_id);
            
            $comision = $movimiento->importe * $regla->comision;
            
            if($movimiento_tipo->tipo == 'debito'){
                $cuenta->saldo = $cuenta->saldo + $movimiento->importe + $comision;

            }elseif($movimiento_tipo->tipo == 'credito'){
                $cuenta->saldo = $cuenta->saldo - $movimiento->importe + $comision;   
            }

            $cuenta->save(); 
        }

        return redirect()->action('HomeController@index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
