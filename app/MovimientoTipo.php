<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovimientoTipo extends Model
{
    protected $table = 'movimiento_tipo';

    protected $fillable = [
        'tipo',
        'descripcion'
    ];

    public function movimientos()
    {
        return $this->hasMany('App\Movimiento');
    }

    public function reglas()
    {
        return $this->hasMany('App\Regla');
    }
}
