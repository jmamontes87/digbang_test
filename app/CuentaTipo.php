<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuentaTipo extends Model
{
    protected $table = 'cuenta_tipo';

    protected $fillable = [
        'denominacion'
    ];

    public function cuentas()
    {
        return $this->hasMany('App\Cuenta');
    }

}
