<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'cliente';

    protected $fillable = [
        'clave_identificacion', //CUIT o CUIL
    	'usuario_id',
    	'tipo_cliente_id'
    ];

    public function usuario()
    {
        return $this->hasOne('App\Usuario', 'id', 'usuario_id');
    }

    public function cuentas()
    {
        return $this->hasMany('App\Cuenta');
    }

}
