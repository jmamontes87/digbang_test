<!DOCTYPE html>
<html>
<head>
	<title>Test Digbang</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<h3>Ingreso de movimientos</h3>
		</div>
	</div>
    {!! Form::open(['route' => 'storeMovimiento', 'method' => 'POST']) !!}
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<div class="form-group">
                {{ Form::select('destino_id', $destinos, null, ['class' => 'form-control border-input','placeholder' => 'Destino', 'required']) }}
			</div>
			<div class="form-group">
                {{ Form::select('movimiento_tipo_id', $movimiento_tipos, null, ['class' => 'form-control border-input','placeholder' => 'Tipo de movimiento', 'required']) }}
			</div>
			<div class="form-group">
                {!! Form::number('importe', null, ['class' => 'form-control border-input', 'placeholder' => 'Importe', 'required']) !!}
			</div>
			<div class="form-group">
                {!! Form::submit('ENVIAR TRANSACCION', ['class'=> 'btn waves-effect waves-light blue lighten-3 black-text']) !!}
			</div>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							Saldo
						</div>
						<div class="col-md-6">
							{{ $cuenta->saldo }}
						</div>
					</div> 
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Importe</th>
								<th>Destino</th>
								<th>Movimiento tipo</th>
							</tr>
						</thead>
						<tbody>
							@if(count($cuenta->movimientos)>0)
								@foreach($cuenta->movimientos as $movimiento)
									<tr>
										<td>{{ $movimiento->importe }}</td>
										<td>{{ $movimiento->destino->destino }}</td>
										<td style="color: {{ $movimiento->movimiento_tipo->tipo == 'credito' ? 'red' : 'black' }}">
											{{ $movimiento->movimiento_tipo->tipo}}
										</td>
									</tr>
								@endforeach
							@endif			
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>


</body>
</html>