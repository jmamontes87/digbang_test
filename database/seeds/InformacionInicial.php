<?php

use Illuminate\Database\Seeder;
use App\Destino;
use App\Regla;
use App\MovimientoTipo;
use App\CuentaTipo;
use App\User;
use App\Cliente;
use App\Cuenta;


class InformacionInicial extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::beginTransaction();
    	try {
    		
    		$cuenta_tipo_classic = new CuentaTipo();
    		$cuenta_tipo_classic->denominacion = 'classic';
    		$cuenta_tipo_classic->save();

    		$cuenta_tipo_gold = new CuentaTipo();
    		$cuenta_tipo_gold->denominacion = 'gold';
    		$cuenta_tipo_gold->save();

	        $destino = new Destino();
	        $destino->destino = 'banelco';
	        $destino->save();

	        $destino = new Destino();
	        $destino->destino = 'link';
	        $destino->save();

	        $destino = new Destino();
	        $destino->destino = 'caja';
	        $destino->save();

	        $movimiento_tipo = new MovimientoTipo();
	        $movimiento_tipo->tipo = 'debito';
	        $movimiento_tipo->save();

	        $dest_banelco = Destino::where('destino','=', 'banelco')->first();
	        $dest_link = Destino::where('destino','=', 'link')->first();
	        $dest_caja = Destino::where('destino','=', 'caja')->first();

	        $regla = new Regla();
	        $regla->denominacion = 'Cobro de comision';
	        $regla->cuenta_tipo_id = $cuenta_tipo_classic->id;
	        $regla->comision = '0.05';
	        $regla->movimiento_tipo_id = $movimiento_tipo->id;
	        $regla->destino_id = $dest_banelco->id;
	        $regla->save();

	        $regla = new Regla();
	        $regla->denominacion = 'Cobro de comision';
	        $regla->cuenta_tipo_id = $cuenta_tipo_classic->id;
	        $regla->comision = '0.01';
	        $regla->destino_id = $dest_link->id;
	        $regla->movimiento_tipo_id = $movimiento_tipo->id;
	        $regla->save();

	        $regla = new Regla();
	        $regla->denominacion = 'Cobro de comision';
	        $regla->cuenta_tipo_id = $cuenta_tipo_gold->id;
	        $regla->comision = '0.05';
	        $regla->destino_id = $dest_link->id;
	        $regla->movimiento_tipo_id = $movimiento_tipo->id;
	        $regla->save();

	        $movimiento_tipo = new MovimientoTipo();
	        $movimiento_tipo->tipo = 'credito';
	        $movimiento_tipo->save();

	        $regla = new Regla();
	        $regla->denominacion = 'Cobro de comision';
	        $regla->cuenta_tipo_id = $cuenta_tipo_classic->id;
	        $regla->comision = '0.05';
	        $regla->destino_id = $dest_banelco->id;
	        $regla->movimiento_tipo_id = $movimiento_tipo->id;
	        $regla->save();

	        $regla = new Regla();
	        $regla->denominacion = 'Cobro de comision';
	        $regla->cuenta_tipo_id = $cuenta_tipo_classic->id;
	        $regla->comision = '0.05';
	        $regla->destino_id = $dest_link->id;
	        $regla->movimiento_tipo_id = $movimiento_tipo->id;
	        $regla->save();

	        $regla = new Regla();
	        $regla->denominacion = 'Cobro de comision';
	        $regla->cuenta_tipo_id = $cuenta_tipo_classic->id;
	        $regla->comision = '0.05';
	        $regla->destino_id = $dest_caja->id;
	        $regla->movimiento_tipo_id = $movimiento_tipo->id;
	        $regla->save();

	        $regla = new Regla();
	        $regla->denominacion = 'Cobro de comision';
	        $regla->cuenta_tipo_id = $cuenta_tipo_gold->id;
	        $regla->comision = '0.03';
	        $regla->destino_id = $dest_banelco->id;
	        $regla->movimiento_tipo_id = $movimiento_tipo->id;
	        $regla->save();

	        $regla = new Regla();
	        $regla->denominacion = 'Cobro de comision';
	        $regla->cuenta_tipo_id = $cuenta_tipo_gold->id;
	        $regla->comision = '0.03';
	        $regla->destino_id = $dest_link->id;
	        $regla->movimiento_tipo_id = $movimiento_tipo->id;
	        $regla->save();

	        $regla = new Regla();
	        $regla->denominacion = 'Cobro de comision';
	        $regla->cuenta_tipo_id = $cuenta_tipo_gold->id;
	        $regla->comision = '0.03';
	        $regla->destino_id = $dest_caja->id;
	        $regla->movimiento_tipo_id = $movimiento_tipo->id;
	        $regla->save();

	        $user_multinacional = new User();
	        $user_multinacional->name = 'multinacional';
	        $user_multinacional->email = "multinacional@gmail.com";
	        $user_multinacional->password = bcrypt('123456');
	        $user_multinacional->save();

	        $cliente = new Cliente();
			$cliente->clave_identificacion = '30326742346';

			$cliente->user_id = $user_multinacional->id;
			$cliente->tipo_cliente_id = 1;
			$cliente->save();

	        $cuenta = new Cuenta();
	        $cuenta->cliente_id = $cliente->id; 
	        $cuenta->cuenta_tipo_id = $cuenta_tipo_classic->id;
			$cuenta->saldo = "500";
			$cuenta->save();

	        $user_pyme = new User();
	        $user_pyme->name = 'pyme';
	        $user_pyme->email = "pyme@gmail.com";
	        $user_pyme->password = bcrypt('123456');
	        $user_pyme->save();

	        $cliente = new Cliente();
			$cliente->clave_identificacion = '300327962676';
			$cliente->user_id = $user_pyme->id;
			$cliente->tipo_cliente_id = 2;
			$cliente->save();

	        $cuenta = new Cuenta();
	        $cuenta->cliente_id = $cliente->id; 
	        $cuenta->cuenta_tipo_id = $cuenta_tipo_gold->id;
			$cuenta->saldo = "500";
			$cuenta->save();

	        $user_persona = new User();
	        $user_persona->name = 'persona';
	        $user_persona->email = "persona@gmail.com";
	        $user_persona->password = bcrypt('123456');
	        $user_persona->save();

	        $cliente = new Cliente();
			$cliente->clave_identificacion = '20327962679';
			$cliente->user_id = $user_persona->id;
			$cliente->tipo_cliente_id = 3;	
			$cliente->save();	
					
	        $cuenta = new Cuenta();
	        $cuenta->cliente_id = $cliente->id; 
			$cuenta->saldo = "500";
			$cuenta->cuenta_tipo_id = $cuenta_tipo_classic->id;
			$cuenta->save();
			
    	} catch (Exception $e) {
    		dd($e);
    		DB::rollBack();
    	}

    	DB::commit();
 
    }
}
