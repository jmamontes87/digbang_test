<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Regla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regla', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('destino_id')->unsigned();
            $table->foreign('destino_id')->references('id')->on('destino');
            $table->integer('movimiento_tipo_id')->unsigned();
            $table->foreign('movimiento_tipo_id')->references('id')->on('movimiento_tipo');
            $table->integer('cuenta_tipo_id')->unsigned();
            $table->foreign('cuenta_tipo_id')->references('id')->on('cuenta_tipo');
            $table->decimal('comision', 5, 2);
            $table->string('denominacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regla');
    }
}
